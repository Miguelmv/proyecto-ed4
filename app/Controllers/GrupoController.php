<?php 
namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\GrupoModel;

class GrupoController extends Controller {
    public  function index(){
        $datos['titol'] ="Listado de Grupos";
        $grupos = new GrupoModel();
        $datos['grupos']= $grupos->findAll();
        echo view('grupos/listagrupos', $datos);
    }
}