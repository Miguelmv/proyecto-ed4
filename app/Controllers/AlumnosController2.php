<?php

    //Ésta es la carpeta dónde se almacena el nuevo Controlador.
    namespace App\Controllers;

    //Aqui lo seleccionamos para enlazarlo con el Modelo.
    use App\Models\AlumnosModel;

    /* Después añadimos la clase con el nuevo controlador
    y lo enlazamos con la base de datos 'centro' y a su vez la tabla 'alumnos'.
    También se tiene que enlazar a su vez con el Modelo y la Vista que hemos creado. */
    class AlumnosController2 extends BaseController {

        public function index() 
        {
           $alumnos = new AlumnosModel();
           $lista ['alumnos'] = $alumnos->findAll();
           echo view('lista', $lista);
        }
         public function alumnos_grupo2($valor="2CFSS") 
         { 
            $alumnos = new AlumnosModel();
            $datos ['titulo'] = "El título que sea";
            $datos ['alumnos'] = $alumnos->select('alumnos.nombre, apellido1, apellido2, email, matricula.grupo')
                ->join('matricula', 'alumnos.NIA = matricula.NIA','LEFT')
                ->where(['grupo' => $valor])
                ->findAll();
             echo view('alumnos/listaalumnosgrupo', $datos);
             
    }
    
}
    