<?php
    //Ésta es la carpeta dónde se almacena el nuevo Modelo.
    namespace App\Models;

    //Aqui lo seleccionamos para enlazarlo con el Controlador.
    use CodeIgniter\Model;

    //Después añadimos la clase con el nuevo modelo y lo enlazamos con la base de datos 'centro' y a su vez la tabla 'alumnos'.
    class AlumnosModel2 extends Model {
    protected $table = 'alumnos';
}


