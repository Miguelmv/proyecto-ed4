<style>
    
      .centro {
        
        margin: auto;
        width: 58%;
    }
    
    #holaa {
        
        border: "1px";
        width: 350 ;
        border-style: solid;
        background-color: #957dad;
    }
    
    

    body {
        
        background-color: #fdc963;
    }
    
    tr {
        
        background-color:white;
    }
    
    tr:nth-child(even) {
        background-color: lightblue;
    }
    
    tr:hover {
        
        background-color: #959695 ;
    }
    
    #tabla {
        
        border: "1px";
        width: 1000 px;
        border-style: solid;
        border-collapse: collapse;
        text-align: center;
    }
    
    .th_class {
        
        border: "1px";
        width: 400 ;
        border-style: solid;
        background-color: #957dad;
    }
    
    .bottom {
        
        border-bottom: "1px";
        border-style: solid;
    }
    
    #boton {
        
        height: 35px;
        width: 200px;
    }
    
    
    
</style>

<body>

<h2 style="text-align: center;">LISTA DE LOS ALUMNOS DEL CENTRO</h2>
<div class="centro">
<table id="tabla">
    <thead>
     <th id="holaa">Nombre</th>
     <th class="th_class">Apellido 1</th>
     <th class="th_class">Apellido 2</th>
     <th class="th_class">Email</th>
     <th class="th_class">Foto</th>
    </thead>
    <tbody>
<?php foreach ($alumnos as $alumno):?>
<p>
    <tr>
    <td class="bottom"><?= $alumno['nombre']?></td>
    <td class="bottom"> <?= $alumno['apellido1']?></td>
    <td class="bottom"> <?= $alumno['apellido2']?></td>
    <td class="bottom"> <?= $alumno['email']?></td>
    <!-- Aquí cargaría las imágenes, pero no hemos añadido la carpeta porque pesa mucho. En local, nos funciona perfectamente, puedes revisarlo mañana en clase-->
    <td class="bottom"> <img src= "<?=base_url('FotosAlu/' . sprintf('%06s', $alumno['id'])) ?>.jpg" width="50px"> </td>
    </tr>
</p>
<?php endforeach; ?>
</tbody>
</table>
</div>
</body>
